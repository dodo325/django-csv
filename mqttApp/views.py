from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
root_dir = "/media/iot/devices/"

# Create your views here.
from django.http import HttpResponse

def index(request):
    template = loader.get_template('mqttApp/index.html')
    c = {}
    return HttpResponse(template.render(c, request))

import csv
def csv_first_line(file_name, n=1): # Чтение первых n строк:
    out = []
    with open(file_name, 'r') as f:
        reader = csv.reader(f)
        for i in range(0,n):
            out.append(next(reader)[0])
    return out

from collections import deque
def csv_last_line(file_name, n=1): # Чтение последних n строк:
    with open(file_name, 'r') as f:
        q = deque(f, n)
    return list(q)

def count_lines_file(file_name): 
    try:
        return sum(1 for line in open(file_name)) # Подсчёт количества
    except Exception as e:
        print(e)

@csrf_exempt
def get_csv(request):
    if(request.method == "GET"):
        res = request.GET.dict()
        file = root_dir
        try:
            file += res['path']
        except Exception: pass

        try:
            a = res["getcount"]
            return HttpResponse(count_lines_file(file))
        except Exception: pass

        try:
            a = res["getnames"]
            import os
            return HttpResponse("{'names': "+str(os.listdir(file))+"}")
        except Exception: pass

        try:
            if(res["getlast"] != ''): return HttpResponse("{ \'rows\': "+str(csv_last_line(file,int(res["getlast"])))+"}")
            else:                    return HttpResponse("{ \'rows\': "+str(csv_last_line(file))+"}")
        except Exception: pass

        try:
            if(res["getfirst"] != ''): return HttpResponse("{ \'rows\': "+str(csv_first_line(file,int(res["getfirst"])))+"}")
            else:                      return HttpResponse("{ \'rows\': "+str(csv_first_line(file))|+"}")
        except Exception: pass

    return HttpResponse('example: csv?path=a1/out.csv&getlast=1 or getfirst=2 or getcount or getnames'+
                        '\n "GET:'+str(res))

